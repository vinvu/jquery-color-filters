# Color-Filters
A jQuery plugin that adds color filters to product filters.

![alt text](https://bitbucket.org/vinvu/jquery-color-filters/src/master/demo.gif "Color Filters Demo")

This plugin was developed by [Vinvu Software](https://vinvu.software).

# Installation

jQuery is required for this plugin. Make sure you have it installed.

Installation is simple, just two files to include. In the head, include the css:

```html
<link href="css/color-filters.css" type="text/css" rel="stylesheet">
```
And then add the js file at the bottom of the page:

```html
<script src="js/color-filters.js" type="text/javascript"></script>
```

# Get Started
The plugin consists of two components on the page:

```html
<div id="color-filters"></div>
```

This is the div where the clickable icons will be put for all the colors. The other component is an optional div that stores the selected colors in tags. Clicking the tags will disable the filter. The id for the div is important:

```html
<div id="activeColorFilters"></div>
```

To initialize the colors, call the "createColors" function on document ready. Hexcodes are required.

```javascript
$(document).ready(function(){

var colors = ["#000000", "#ffffff"];

createColorFilters(colors);

});
```
You're all setup!

# Accessing the selected colors

To access the actively selected color filters, just call the array in javascript and do with it what you want:

```javascript
for (var index in selectedColors) {
    var color = selectedColors[index];
    alert(color);
}
```
