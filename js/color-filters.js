/**
 * Created by vinjo on 18/07/2017.
 */

var selectedColors = [];

function createColorFilters(colors) {
    for(var index in colors) {
        $('#color-filters').append(
            '<div class="color-filter-parent"><div class="color-filter" onclick="toggleColor(\'' + colors[index] + '\'); toggleSelected(this);" style="background-color: ' + colors[index] + '"></div>'
        );
    }
}

function toggleColor(color) {

    if (selectedColors.indexOf(color) >= 0) {
        removeColorFilter(color)
    } else if (selectedColors.indexOf(color) === -1) {
        addColorFilter(color)
    }
}

function addColorFilter(color) {
    selectedColors.push(color);
    injectActiveColorFilters();
}

function removeColorFilter(color) {
    selectedColors.splice(selectedColors.indexOf(color), 1);
    injectActiveColorFilters();
}

function injectActiveColorFilters () {

    $('#activeColorFilters').empty();

    for (var index in selectedColors) {
        var color = selectedColors[index];

        $('#activeColorFilters').append(
            '<a onclick="toggleColor(\'' + color + '\');turnOffToggle(\'' + color + '\')" class="activeFilter"><span class="color-filter-active" style="background-color:'+ color +'"></span> &times;</a>'
        );
    }
}

//ADD SELECTED CLASS FOR STYLING PURPOSES

function toggleSelected(element) {
    $(element).toggleClass('color-selected');
}

function turnOffToggle(color) {

    var elements = document.getElementsByClassName('color-filter');

    for(var index in elements) {

        var element = elements[index];

        if (element.style !== undefined) {

            if (color === rgb2hex(element.style.backgroundColor)) {
                toggleSelected(element);
            }
        }
    }

}

//Function to convert rgb color to hex format
function rgb2hex(rgb) {

    var hexDigits = ["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"];

    function hex(x) {
        return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
    }

    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}